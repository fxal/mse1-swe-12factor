# MSE1 SWE 12FACTOR Exercise

A micro service programming exercise in [rust](https://www.rust-lang.org/) following the [12factor](https://12factor.net/) architectural guidelines.

This micro service starts a web server listening on http://localhost:8000 and will present a julia fractal generator and a greeter.

A summary of the 12 factors and their usage in this project can be found [here](factors.md).

## How To start
```docker-compose -f docker-compose.yaml up -d```

This docker container listens on http://localhost:8000

## Purpose
The micro service will show 
- a fractal generator for a julia fractal
- a name greeter with an input text field

## Stack
- docker, docker compose
- [rust nightly docker image](https://hub.docker.com/r/rustlang/rust/tags)
- some plain old html
- frameworks:
    - [rocket](https://rocket.rs/)
    - [log4rs](https://github.com/estk/log4rs)
- [julia fractal generator](https://github.com/image-rs/image#generating-fractals)