FROM rustlang/rust:nightly-stretch-slim

EXPOSE 8000

WORKDIR /usr/src/twelvefactor
COPY . .

RUN rustc --version
RUN cargo install --path .
CMD ["twelvefactor"]