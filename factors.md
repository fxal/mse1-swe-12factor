# 12 FACTOR

Factors implemented in this project of http://12factor.net

## 1. Codebase

This project is under git version control. See attached `gitlog.txt`

## 2. Dependencies

Dependencies are declared clearly in this projects `Cargo.toml`

## 3. Config

Due to this project being containerized with docker-compose, environment variables can be given to the program for configuration purposes.
Although this project has no sensitive configuration such as data base credentials, it could be provided via `ENV`.

## 4. Backing services

This project uses no backing service to generate a julia fractal. It could separate the two services `Generate Julia Fractal` and `Greet with name` into
three rust container:
- one container to serve a landing page
- one container to serve the `Greet with name` service
- one container for serving the `Generate Julia Fractal`

Due to limited hardware I chose to have a single container run these three parts

## 5. Build, release, run

The docker methodology takes care of this. There are several stages during container creation:

> Step 1/7 : FROM rustlang/rust:nightly-stretch-slim
>
> Step 2/7 : EXPOSE 8000
>
> Step 3/7 : WORKDIR /usr/src/twelvefactor
>
> Step 4/7 : COPY . .
>
>Step 5/7 : RUN rustc --version
>
>Step 6/7 : RUN cargo install --path .
>
>Step 7/7 : CMD ["twelvefactor"]

## 6. Processes

The docker methodology also takes care of this. A docker container encapsulates and separates.
This application itself is also stateless.

## 7. Port binding

Port binding is handled by docker and docker-compose. The rocket framework exposes the port 8000 by default.

## 8. Concurrency

Given a load balancer this application can be scaled without any issues due to being in a docker container.

## 9. Disposability

Once the image is built of this application, startup and shutdown are graceful and fast.

## 10. Dev/prod parity

Due to being within a docker container, this application is as close to production as possible at any time.

## 11. Logs

This application uses the logging framework `log4rs` that allows for stdout to be a sink. This is the configuration for this project.

## 12. Admin processes

There are no admin processes within this application. They could be integrated with the authentication features of the `rocket` framework.