extern crate image;
extern crate num_complex;

use num::integer::Roots;

// Taken from https://github.com/image-rs/image
pub fn julia(width: u32, height: u32, scale: u32) -> String {

    info!("Creating Julia fractal from arguments width={} height={} scale={}", width, height, scale);

    let image_name = "julia_fractal.png";

    let scale_x = scale as f32 / width as f32;
    let scale_y = scale as f32 / height as f32;

    let mut julia_image = image::ImageBuffer::new(width, height);

    for (x, y, pixel) in julia_image.enumerate_pixels_mut() {
        let r = (0.3 * x as f32) as u8;
        let b = (0.3 * y as f32) as u8;

        let z = x^2 + y^2;
        let g = (0.3 * z.sqrt() as f32) as u8;
        *pixel = image::Rgb([r, g, b]);
    }

    for x in 0..width {
        for y in 0..height {
            let cx = y as f32 * scale_x - 1.5;
            let cy = x as f32 * scale_y - 1.5;

            let c = num_complex::Complex::new(-0.4, 0.6);
            let mut z = num_complex::Complex::new(cx, cy);

            let mut i = 0;
            while i < 255 && z.norm() <= 2.0 {
                z = z * z + c;
                i += 1;
            }

            let pixel = julia_image.get_pixel_mut(x, y);
            let data= *pixel;
            *pixel = image::Rgb([data[0], i as u8, data[2]]);
        }
    }

    julia_image.save(image_name);
    return image_name.to_owned();
}