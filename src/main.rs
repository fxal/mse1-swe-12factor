#![feature(proc_macro_hygiene, decl_macro)]

mod julia;

#[macro_use] extern crate log;
extern crate log4rs;
extern crate log4rs_rolling_file;
#[macro_use] extern crate rocket;
extern crate num;

use rocket::response::NamedFile;
use std::path::Path;

pub fn main() {
    init_logger();
    info!("Hello logfile!");
    println!("Hello console!");
    rocket::ignite()
        .mount("/", routes![hello, fractals, julia])
        .launch();
}

#[get("/hello?<name>")]
pub fn hello(name: String) -> String {
    info!("Received hello request from {}", name);
    return std::format!("Hello, {}", name);
}

#[get("/")]
pub fn fractals() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/index.html")).ok()
}

#[get("/julia?<width>&<height>&<scale>")]
pub fn julia(width: u32, height: u32, scale: u32) -> Option<NamedFile> {
    NamedFile::open(Path::new(julia::julia(width, height, scale).as_str())).ok()
}

fn init_logger() {
    use log4rs::init_file;
    use log4rs::file::Deserializers;
    let mut deserializers = Deserializers::default();
    log4rs_rolling_file::register(&mut deserializers);
    init_file("logging-configuration.yaml", deserializers).unwrap();
}